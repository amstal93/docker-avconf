FROM debian:jessie

RUN echo "deb http://ftp.us.debian.org/debian jessie main contrib" >> /etc/apt/sources.list
RUN apt-get -q update && \
    apt-get install -qy \
                    libav-tools \
                    python3 \
                    python3-pip \
                    fonts-dejavu-core && \
    rm -rf /var/lib/apt/lists/*

# so you can emboss high quality text into your lame stream
RUN mkdir -p /usr/share/fonts/dejavu/ && \
    ln -s /usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf /usr/share/fonts/dejavu/DejaVuSans-Bold.ttf

WORKDIR /root
ADD start_avconv.sh .
RUN chmod +x start_avconv.sh

ENTRYPOINT [ "sh", "start_avconv.sh" ]
